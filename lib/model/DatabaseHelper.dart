import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_app/model/todo.dart';
import 'package:path/path.dart';
class DatabaseHelper {
  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database? _database;

  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'todo.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE todo(
      id INTEGER PRIMARY KEY,
      content TEXT,
      status INTERGER
    )
    ''');
  }

  Future<List<Todo>> getTodo() async {
    Database db = await instance.database;
    var todo = await db.query('todo', orderBy: 'id');
    List<Todo> todoList =
    todo.isNotEmpty ? todo.map((c) => Todo.fromMap(c)).toList() : [];
    return todoList;
  }

  Future<int> add(Todo todo) async {
    Database db = await instance.database;
    return await db.insert('todo', todo.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('todo', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(Todo todo) async {
    Database db = await instance.database;
    return await db
        .update('todo', todo.toMap(), where: 'id = ?', whereArgs: [todo.id]);
  }

  Future<int> changstatus(Todo todo) async {
    Database db = await instance.database;
    return await db
        .update('todo', todo.toMap(), where: 'id = ?', whereArgs: [todo.id]);
  }
}
