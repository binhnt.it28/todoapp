import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class Todo {
  final int? id;
  final String content;
  final int? status;
  Todo({this.id, required this.content, this.status});
  factory Todo.fromMap(Map<String, dynamic> json) => new Todo(
      id: json['id'], content: json['content'], status: json['status']);
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'content': content,
      'status': status,
    };
  }
}


