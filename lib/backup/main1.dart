import 'dart:io';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _text = '';
  // pref.setInt('data',1);
  int? selected;
  int? total;
  int? status;
  final textController = TextEditingController();
  _loadLocal() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _text = prefs.getString('data') ?? '';
    });
    print(_text);
    textController.text = _text;
  }
  void _setLocal() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _text = prefs.getString('data') ?? '';
      prefs.setString('data', textController.text);
    });
    print(_text);
  }
  // final myController = TextEditingController();
  @override
  void initState() {
    super.initState();
    _loadLocal();
    setState(() {
      textController.text;
    });
    // Start listening to changes.
    textController.addListener(_printLatestValue);
  }
  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    textController.dispose();
    super.dispose();
  }
  void _printLatestValue() {
    setState(() {
      _setLocal();
      textController.text;
    });
    // print(textController.text);
  }
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Center(child: Text("Todo")),
        ),
        floatingActionButton: selected!=null?FloatingActionButton(
          child:Icon(Icons.add),
          onPressed: () async{
            if (_formKey.currentState!.validate()) {
              total = total! + 1;
              await DatabaseHelper.instance.add(
                Todo(content: textController.text, status: 0),
              );
              setState(() {
                total;
                textController.clear();
                selected = null;
              });
            }
          },
        ):null,
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                      child: Form(
                          key: _formKey,
                          child: TextFormField(
                            controller: textController,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Vui lòng nhập';
                              }
                              return null;
                            },
                          ))),
                  SizedBox(
                    width: 20,
                  ),
                  ElevatedButton(
                      child: selected == null ? Text("ADD") : Text("Update"),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          selected != null
                              ? await DatabaseHelper.instance.update(
                            Todo(
                                id: selected,
                                content: textController.text,
                                status: status),
                          )
                              : total = total! + 1;
                          await DatabaseHelper.instance.add(
                            Todo(content: textController.text, status: 0),
                          );
                          setState(() {
                            total;
                            textController.clear();
                            selected = null;
                          });
                        }
                      }),

                ],
              ),
              SizedBox(
                height: 20,
              ),
              Expanded(
                child: FutureBuilder<List<Todo>>(
                  future: DatabaseHelper.instance.getTodo(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<Todo>> snapshot) {
                    if (snapshot.hasData) {
                      total = snapshot.data!.length;
                      return snapshot.data!.isEmpty
                          ? Center(
                        child: Text("No Todo"),
                      )
                          : ListView(
                        children: snapshot.data!.map((todo) {
                          return Row(children: <Widget>[
                            TextButton(
                                onPressed: () async {
                                  todo.status == 0
                                      ? await DatabaseHelper.instance
                                      .changstatus(
                                    Todo(
                                        id: todo.id,
                                        content: todo.content,
                                        status: 1),
                                  )
                                      : await DatabaseHelper.instance
                                      .changstatus(
                                    Todo(
                                        id: todo.id,
                                        content: todo.content,
                                        status: 0),
                                  );
                                  setState(() {
                                    // textController.clear();
                                    // selected=null;
                                  });
                                },
                                child: Icon(todo.status == 0
                                    ? Icons.crop_3_2_rounded
                                    : Icons.check_box_outlined)),
                            todo.id==selected?
                            Ink(
                              color: Colors.blue,
                              width:250,
                              child: ListTile(
                                title: Text(
                                  todo.content,
                                  style: todo.status != 0
                                      ?
                                  todo.id ==selected ?
                                  TextStyle(
                                      color: Colors.white,
                                      decoration:
                                      TextDecoration.lineThrough,
                                      fontSize: 20):
                                  TextStyle(
                                      color: Colors.red,
                                      decoration:
                                      TextDecoration.lineThrough,
                                      fontSize: 20)
                                      :
                                  todo.id ==selected ?
                                  TextStyle(
                                      color: Colors.white,
                                      fontSize: 20):
                                  TextStyle(
                                      color: Colors.black87,
                                      fontSize: 20),
                                ),
                                onTap: () {
                                  setState(() {
                                    if (selected == null) {
                                      textController.text = todo.content;
                                      selected = todo.id;
                                      status = todo.status;
                                    }
                                    else
                                    {
                                      textController.text = '';
                                      selected = null;
                                    }
                                  });
                                },
                              ),
                            ):
                            Ink(
                              width:250,
                              child: ListTile(
                                title: Text(
                                  todo.content,
                                  style: todo.status != 0
                                      ?
                                  todo.id ==selected ?
                                  TextStyle(
                                      color: Colors.red,
                                      backgroundColor: Colors.red,
                                      decoration:
                                      TextDecoration.underline,
                                      fontSize: 20):
                                  TextStyle(
                                      color: Colors.red,
                                      decoration:
                                      TextDecoration.lineThrough,
                                      fontSize: 20)
                                      : TextStyle(
                                      color: Colors.black87,
                                      fontSize: 20),
                                ),
                                onTap: () {
                                  setState(() {
                                    if (selected == null) {
                                      textController.text = todo.content;
                                      selected = todo.id;
                                      status = todo.status;
                                    } else {
                                      textController.text = '';
                                      selected = null;
                                    }
                                  });
                                },
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                                DatabaseHelper.instance.remove(todo.id!);
                                total = total! - 1;
                                setState(() {
                                  total;
                                });
                              },
                              child: Icon(
                                Icons.delete,
                                color: Colors.red,
                              ),
                            ),
                          ]);
                        }).toList(),
                      );
                    }
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                    // total = snapshot.length;
                  },
                ),
              ),
              Center(
                child: Text(
                  "Tổng việc: $total",
                  style: TextStyle(fontSize: 20),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
class Todo {
  final int? id;
  final String content;
  final int? status;
  Todo({this.id, required this.content, this.status});
  factory Todo.fromMap(Map<String, dynamic> json) => new Todo(
      id: json['id'], content: json['content'], status: json['status']);
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'content': content,
      'status': status,
    };
  }
}
class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();
  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'todo.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }
  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE todo(
      id INTEGER PRIMARY KEY,
      content TEXT,
      status INTERGER
    )
    ''');
  }

  Future<List<Todo>> getTodo() async {
    Database db = await instance.database;
    var todo = await db.query('todo', orderBy: 'id');
    List<Todo> todoList =
    todo.isNotEmpty ? todo.map((c) => Todo.fromMap(c)).toList() : [];
    return todoList;
  }
  Future<int> add(Todo todo) async {
    Database db = await instance.database;
    return await db.insert('todo', todo.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('todo', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(Todo todo) async {
    Database db = await instance.database;
    return await db
        .update('todo', todo.toMap(), where: 'id = ?', whereArgs: [todo.id]);
  }

  Future<int> changstatus(Todo todo) async {
    Database db = await instance.database;
    return await db
        .update('todo', todo.toMap(), where: 'id = ?', whereArgs: [todo.id]);
  }
}
