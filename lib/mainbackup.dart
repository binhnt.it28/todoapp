import 'dart:io';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int? selected;

  final textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title:Center( child: TextField(controller: textController,)) ,
        ),
        body: Center(
          child: FutureBuilder<List<Todo>>(
            future: DatabaseHelper.instance.getTodo(),
            builder: (BuildContext context,AsyncSnapshot<List<Todo>> snapshot){
              if (!snapshot.hasData){
                return Center(child:  Text("Loading"),);
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text("No Todo"),):
              ListView(
                children: snapshot.data!.map((todo){
                  return Center(
                    child: ListTile(
                      title:Text(todo.content),
                      onTap: (){
                        setState(() {
                          if (selected==null) {
                            textController.text = todo.content;
                            selected = todo.id;
                          }else {
                            textController.text = '';
                            selected =null;
                          }
                        });
                      },
                      onLongPress: (){
                        setState(() {
                          DatabaseHelper.instance.remove(todo.id!);
                        });
                      },
                    ),
                  );
                }).toList(),
              );
            },
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.save),
          onPressed: ()async{
            selected!=null? await DatabaseHelper.instance.update(
              Todo(id: selected,content: textController.text),
            )

                :
            await DatabaseHelper.instance.add(
              Todo(content:textController.text),
            );
            setState(() {
              textController.clear();
              selected=null;
            });
          },
        ),
      ),
    );
  }
}
class Todo {
  final int? id;
  final String content;
  Todo({this.id,required this.content});
  factory Todo.fromMap(Map<String,dynamic> json) =>new Todo(
    id : json['id'],
    content: json['content'],
  );
  Map<String,dynamic> toMap(){
    return {
      'id':id,
      'content':content,
    };
  }
}
class DatabaseHelper{
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database? _database;
  Future<Database> get database async =>_database ??=await _initDatabase();
  Future<Database> _initDatabase() async{
    Directory  documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path,'todo.db');
    return await openDatabase(
      path,
      version:1,
      onCreate:_onCreate,
    );
  }
  Future _onCreate(Database db, int version) async{
    await  db.execute('''
      CREATE TABLE todo(
      id INTEGER PRIMARY KEY,
      content TEXT
    )
    ''');
  }
  Future<List<Todo>> getTodo() async{
    Database db = await instance.database;
    var todo = await db.query('todo',orderBy: 'id');
    List<Todo> todoList = todo.isNotEmpty?todo.map((c)=> Todo.fromMap(c)).toList():[];
    return todoList;

  }
  Future<int> add(Todo todo) async{
    Database db = await instance.database;
    return await db.insert('todo', todo.toMap());
  }
  Future<int> remove(int id) async{
    Database db = await instance.database;
    return await db.delete('todo',where: 'id = ?',whereArgs: [id]);
  }
  Future<int> update(Todo todo)async{
    Database db = await instance.database;
    return await db.update('todo',todo.toMap(),where:'id = ?',whereArgs: [todo.id] );
  }
}
