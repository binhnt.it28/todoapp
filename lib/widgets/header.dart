import 'package:flutter/material.dart';
import 'package:todo_app/model/DatabaseHelper.dart';
import 'package:todo_app/model/todo.dart';

class Header extends StatefulWidget {
  const Header({Key? key}) : super(key: key);

  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  int? selected;
  int? total;
  int? status;
  final textController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return  Row(
      children: [
        Expanded(
            child: Form(
                key: _formKey,
                child: TextFormField(
                  controller: textController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Vui lòng nhập';
                    }
                    return null;
                  },
                ))),
        SizedBox(
          width: 20,
        ),
        ElevatedButton(
            child: selected == null ? Text("ADD") : Text("Update"),
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                selected != null
                    ? await DatabaseHelper.instance.update(
                  Todo(
                      id: selected,
                      content: textController.text,
                      status: status),
                )
                    : total = total! + 1;
                await DatabaseHelper.instance.add(
                  Todo(content: textController.text, status: 0),
                );
                setState(() {
                  total;
                  textController.clear();
                  selected = null;
                });
              }
            }),
      ],
    );
  }
}
