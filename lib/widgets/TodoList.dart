import 'package:flutter/material.dart';
import 'package:todo_app/model/DatabaseHelper.dart';
import 'package:todo_app/model/todo.dart';

class TodoList extends StatefulWidget {

  @override
  _State createState() => _State();
}

class _State extends State<TodoList> {
  int? selected;
  int? total;
  int? status;
  final textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FutureBuilder<List<Todo>>(
        future: DatabaseHelper.instance.getTodo(),
        builder: (BuildContext context,
            AsyncSnapshot<List<Todo>> snapshot) {
          if (snapshot.hasData) {
            total = snapshot.data!.length;
            return snapshot.data!.isEmpty
                ? Center(
              child: Text("No Todo"),
            )
                :
            ListView(
              children: snapshot.data!.map((todo) {
                return Row(children: <Widget>[
                  TextButton(
                      onPressed: () async {
                        todo.status == 0
                            ? await DatabaseHelper.instance
                            .changstatus(
                          Todo(
                              id: todo.id,
                              content: todo.content,
                              status: 1),
                        )
                            : await DatabaseHelper.instance
                            .changstatus(
                          Todo(
                              id: todo.id,
                              content: todo.content,
                              status: 0),
                        );
                        setState(() {
                          // textController.clear();
                          // selected=null;
                        });
                      },
                      child: Icon(todo.status == 0
                          ? Icons.crop_3_2_rounded
                          : Icons.check_box_outlined)),
                  todo.id == selected ?
                  Ink(
                    color: Colors.blue,
                    width: 250,
                    child: ListTile(
                      title: Text(
                        todo.content,
                        style: todo.status != 0
                            ?
                        todo.id == selected ?
                        TextStyle(
                            color: Colors.white,
                            decoration:
                            TextDecoration.lineThrough,
                            fontSize: 20) :
                        TextStyle(
                            color: Colors.red,
                            decoration:
                            TextDecoration.lineThrough,
                            fontSize: 20)
                            :
                        todo.id == selected ?
                        TextStyle(
                            color: Colors.white,
                            fontSize: 20) :
                        TextStyle(
                            color: Colors.black87,
                            fontSize: 20),
                      ),
                      onTap: () {
                        setState(() {
                          if (selected == null) {
                            textController.text = todo.content;
                            selected = todo.id;
                            status = todo.status;
                          }
                          else {
                            textController.text = '';
                            selected = null;
                          }
                        });
                      },
                    ),
                  ) :
                  Ink(
                    width: 250,
                    child: ListTile(
                      title: Text(
                        todo.content,
                        style: todo.status != 0
                            ?
                        todo.id == selected ?
                        TextStyle(
                            color: Colors.red,
                            backgroundColor: Colors.red,
                            decoration:
                            TextDecoration.underline,
                            fontSize: 20) :
                        TextStyle(
                            color: Colors.red,
                            decoration:
                            TextDecoration.lineThrough,
                            fontSize: 20)
                            : TextStyle(
                            color: Colors.black87,
                            fontSize: 20),
                      ),
                      onTap: () {
                        setState(() {
                          if (selected == null) {
                            textController.text = todo.content;
                            selected = todo.id;
                            status = todo.status;
                          } else {
                            textController.text = '';
                            selected = null;
                          }
                        });
                      },
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      DatabaseHelper.instance.remove(todo.id!);
                      total = total! - 1;
                      setState(() {
                        total;
                      });
                    },
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                  ),
                ]);
              }).toList(),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
          // total = snapshot.length;
        },
      ),
    );
  }
}